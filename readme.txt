script:     dynamic_field_mapping.py
author:     scorvene & nraphael
date:       2016-03-02
function:   Module. When imported into a script, allows script to process Excel spreadsheets in which columns are not
            in rigid order.  Without this module, scripts specify exact order of columns.  This was causing problems.
            This script was written to solve those problems.

script:     industries-rdf.py
author:     scorvene
date:       2016-05-19
function:   Converts Excel spreadsheet of industry data into RDF.  Uses mapping of industries to NAICS codes and
            includes the NAICS codes as URI references

            Input has taxonomy terms arranged in hierarchy and definitions on one tab, NAICS mapping on a separate tab

            Sample output:
            hbs:Industry-05e24dbe-c33f-546a-ba78-8ebc917c0093 a <http://www.w3.org/2002/07/owl#Class> ;
                rdfs:label "Marine Industry" ;
                hbs:hasBroaderTerm hbs:Industry-f33a21b2-17de-549e-8cfe-9aec9b15f2c7 ;
                hbs:hasDescription "The Marine Industry in the Industrials Sector includes companies providing goods or
                passenger maritime transportation. It does not include cruise ships classified in the Hotels, Resorts
                and Cruise Lines industry." ;
                hbs:hasUniqueLabel "Marine_Industry" ;
                hbs:mapsTo <http://purl.org/weso/pscs/naics/2012/resource/3366>,
                    <http://purl.org/weso/pscs/naics/2012/resource/33661>,
                    <http://purl.org/weso/pscs/naics/2012/resource/336611>,
                    <http://purl.org/weso/pscs/naics/2012/resource/336612>,
                    <http://purl.org/weso/pscs/naics/2012/resource/483>,
                    <http://purl.org/weso/pscs/naics/2012/resource/4831>,
                    <http://purl.org/weso/pscs/naics/2012/resource/48311>,
                    <http://purl.org/weso/pscs/naics/2012/resource/483111>,
                    <http://purl.org/weso/pscs/naics/2012/resource/483112>,
                    <http://purl.org/weso/pscs/naics/2012/resource/483113>,
                    <http://purl.org/weso/pscs/naics/2012/resource/483114>,
                    <http://purl.org/weso/pscs/naics/2012/resource/4832>,
                    <http://purl.org/weso/pscs/naics/2012/resource/48321>,
                    <http://purl.org/weso/pscs/naics/2012/resource/483211>,
                    <http://purl.org/weso/pscs/naics/2012/resource/483212> .

script:     industries-rdf-with-alt-labels.py
author:     scorvene
date:       2016-06-02
function:   Same as industries-rdf.py with these additions:
            Adds alternate labels as text strings
                hbs:hasAlternateLabel "books",
                "comic books",
                "magazines",
                "newspapers" ;
            Adds root element of "Industries"
            hbs:Industry-5395bfa4-13d4-59e0-9c1d-47ae7e9dd2dd a <http://www.w3.org/2002/07/owl#Class> ;
                rdfs:label "Industries" .