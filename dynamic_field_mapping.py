import xlrd
import openpyxl
import string
from openpyxl import load_workbook


def map_fields_to_column_headers(row):
    headers_to_columns = {}
    for cell in row:
        if cell.value is not None:
            new_cell_value = cell.value.replace(' ', '_')
        if new_cell_value is not None and cell.value is not None:
            headers_to_columns[new_cell_value.strip()] = cell.column

    return headers_to_columns


def main():
    workbook = load_workbook("", use_iterators=True)
    first_sheet = workbook.get_sheet_names()[0]
    worksheet = workbook.get_sheet_by_name(first_sheet)

    i = 0
    for row in worksheet.iter_rows():
        i += 1
        if i == 1:
            headers_to_columns = map_fields_to_column_headers(row)
        else:
            break
    print headers_to_columns

if __name__ == "__main__":
    main()
