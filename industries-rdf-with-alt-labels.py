import argparse
from openpyxl import load_workbook
from rdflib import Graph, Literal, Namespace
from rdflib.namespace import RDF, RDFS, OWL, URIRef
import uuid
import dynamic_field_mapping
import re

__author__ = 'scorvene'

parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--input-file', dest='industry_input', required=True,
                    help="Excel file to be converted into triples.")
parser.add_argument('--output-file', dest='industry_output', required=True,
                    help="Turtle file of industry triples")
parser.add_argument('--industry-worksheet', dest='industry_worksheet', required=True,
                    help="Name of Excel spreadsheet tab with industry data")
parser.add_argument('--naics-worksheet', dest='naics_worksheet', required=True,
                    help="Name of Excel spreadsheet tab with NAICS mapping")

arguments = parser.parse_args()

industry_worksheet = arguments.industry_worksheet
naics_worksheet = arguments.naics_worksheet
industry_input = arguments.industry_input
industry_output = arguments.industry_output

cols_by_name = ""
NONE = unicode('none')

graph = Graph()
HBS = Namespace("http://data.hbs.edu/")
graph.bind("hbs", HBS)


def get_sheet_value_for_column_name_at_row_y(sheet, column_name, row_y):
    val = sheet[cols_by_name[column_name] + str(row_y)].value
    return val


def create_uuid(u_label):
    unique_label = u_label.replace(' ', '')
    uuid_val = uuid.uuid5(uuid.NAMESPACE_DNS, (str(unique_label)))
    hbs_industry_uri = HBS.term("Industry-" + str(uuid_val))
    return hbs_industry_uri


workbook = load_workbook(industry_input)
naics_sheet = workbook.get_sheet_by_name(naics_worksheet)
num_rows = naics_sheet.max_row + 1

naics_uri_start = 'http://purl.org/weso/pscs/naics/2012/resource/'

naics_dict = dict()

# build naics uri dictionary
for row_y in range(1, num_rows):
    if row_y == 1:
        cols_by_name = dynamic_field_mapping.map_fields_to_column_headers(naics_sheet.rows[0])
        continue

    level_3 = get_sheet_value_for_column_name_at_row_y(naics_sheet, 'Level3', row_y)
    industry_uuid = create_uuid(level_3)
    code = get_sheet_value_for_column_name_at_row_y(naics_sheet, 'code', row_y)

    if industry_uuid not in naics_dict:
        code_list = list()
        code_list.append(str(code))
        naics_dict[industry_uuid] = code_list
    else:
        naics_dict[industry_uuid].append(code)

industry_sheet = workbook.get_sheet_by_name(industry_worksheet)

industries_uuid = create_uuid('Industries')
graph.add((industries_uuid, RDF.type, OWL.Class))
graph.add((industries_uuid, RDFS.label, Literal('Industries')))

for row_y in range(1, num_rows):
    if row_y == 1:
        cols_by_name = dynamic_field_mapping.map_fields_to_column_headers(industry_sheet.rows[0])
        continue

    l3 = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'Level3', row_y)
    l2 = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'Level2', row_y)
    l1 = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'Level1', row_y)

    if l3 is not None:
        uuid_3 = create_uuid(l3)
        graph.add((uuid_3, RDF.type, OWL.Class))
        industry_label = l3.replace('_', ' ')
        graph.add((uuid_3, RDFS.label, Literal(industry_label)))

        industry_description = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'description', row_y)
        if industry_description is not None:
            industry_description = industry_description.strip()
            graph.add((uuid_3, HBS.term('hasDescription'), Literal(industry_description)))

        uuid_2 = create_uuid(l2)
        graph.add((uuid_3, HBS.term('hasBroaderTerm'), uuid_2))
        graph.add((uuid_3, HBS.term('hasUniqueLabel'), Literal(l3)))

        if uuid_3 in naics_dict:
            for c in naics_dict[uuid_3]:
                naics_uri = naics_uri_start + str(c)
                graph.add((uuid_3, HBS.term('mapsTo'), URIRef(naics_uri)))

        alternate_label = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'Alternate_Labels', row_y)
        if alternate_label is not None:
            if re.search(';', alternate_label):
                syn_list = alternate_label.split(';')
                syn_counter = 0
                length = len(syn_list)
                while syn_counter < length:
                    syn = syn_list[syn_counter]
                    sn = syn.strip()
                    graph.add((uuid_3, HBS.term('hasAlternateLabel'), Literal(sn)))
                    syn_counter += 1
            else:
                alt = alternate_label.strip()
                graph.add((uuid_3, HBS.term('hasAlternateLabel'), Literal(alt)))

    elif l2 is not None:
        uuid_2 = create_uuid(l2)
        graph.add((uuid_2, RDF.type, OWL.Class))
        industry_label = l2.replace('_', ' ')
        graph.add((uuid_2, RDFS.label, Literal(industry_label)))

        uuid_1 = create_uuid(l1)
        graph.add((uuid_2, HBS.term('hasBroaderTerm'), uuid_1))
        graph.add((uuid_2, HBS.term('hasUniqueLabel'), Literal(l2)))

        industry_description = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'description', row_y)
        if industry_description is not None:
            industry_description = industry_description.strip()
            graph.add((uuid_2, HBS.term('hasDescription'), Literal(industry_description)))

        if uuid_2 in naics_dict:
            for c in naics_dict[uuid_2]:
                naics_uri = naics_uri_start + str(c)
                graph.add((uuid_2, HBS.term('mapsTo'), URIRef(naics_uri)))

        alternate_label = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'Alternate_Labels', row_y)
        if alternate_label is not None:
            if re.search(';', alternate_label):
                syn_list = alternate_label.split(';')
                syn_counter = 0
                length = len(syn_list)
                while syn_counter < length:
                    syn = syn_list[syn_counter]
                    sn = syn.strip()
                    graph.add((uuid_2, HBS.term('hasAlternateLabel'), Literal(sn)))
                    syn_counter += 1
            else:
                alt = alternate_label.strip()
                graph.add((uuid_2, HBS.term('hasAlternateLabel'), Literal(alt)))

    elif l1 is not None:
        uuid_1 = create_uuid(l1)
        graph.add((uuid_1, RDF.type, OWL.Class))
        industry_label = l1.replace('_', ' ')
        graph.add((uuid_1, RDFS.label, Literal(industry_label)))

        industries_uuid = create_uuid('Industries')
        graph.add((uuid_1, HBS.term('hasBroaderTerm'), industries_uuid))
        graph.add((uuid_1, HBS.term('hasUniqueLabel'), Literal(l1)))

        industry_description = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'description', row_y)
        if industry_description is not None:
            industry_description = industry_description.strip()
            graph.add((uuid_1, HBS.term('hasDescription'), Literal(industry_description)))

        alternate_label = get_sheet_value_for_column_name_at_row_y(industry_sheet, 'Alternate_Labels', row_y)
        if alternate_label is not None:
            if re.search(';', alternate_label):
                syn_list = alternate_label.split(';')
                syn_counter = 0
                length = len(syn_list)
                while syn_counter < length:
                    syn = syn_list[syn_counter]
                    sn = syn.strip()
                    graph.add((uuid_1, HBS.term('hasAlternateLabel'), Literal(sn)))
                    syn_counter += 1
            else:
                alt = alternate_label.strip()
                graph.add((uuid_1, HBS.term('hasAlternateLabel'), Literal(alt)))



graph.serialize(destination=industry_output, format="turtle")
